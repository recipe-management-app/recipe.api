﻿using System.ComponentModel.DataAnnotations;

namespace Recipe.API.Models
{
    public class Ingredient : Base
    {
        [Required]
        public string RecipeId { get; set; }

        [Required, MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [Required, Range(1, int.MaxValue)]
        public int Qty { get; set; }

        [Required]
        public Unit Unit { get; set; }

        public Recipe Recipe { get; set; }
    }

    public enum Unit
    {
        Gr,
        KG,
        ML,
        L
    }
}
