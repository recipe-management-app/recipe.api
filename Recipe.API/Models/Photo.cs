﻿using System.ComponentModel.DataAnnotations;

namespace Recipe.API.Models
{
    public class Photo : Base
    {
        [Required]
        public string RecipeId { get; set; }

        [Required]
        public string Url { get; set; }

        public Recipe Recipe { get; set; }
    }
}
