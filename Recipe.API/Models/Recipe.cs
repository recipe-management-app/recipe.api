﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Recipe.API.Models
{
    public class Recipe : Base
    {
        /* Required be EF */
        public Recipe()
        {
        }

        [Required, MinLength(3), MaxLength(100)]
        public string Name { get; set; }
        
        [Required]
        public Type Type { get; set; }
        
        [Required]
        public string Description { get; set; }

        public virtual ICollection<Photo> Photos { get; set; }

        public virtual ICollection<Step> Steps { get; set; }

        public virtual ICollection<Ingredient> Ingredients { get; set; }
    }

    public enum Type
    {
        MainCourse,
        Dessert,
        Appetizer,
        Drink
    }
}
