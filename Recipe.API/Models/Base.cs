﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Recipe.API.Models
{
    public class Base
    {
        [Required]
        public string Id { get; set; } = Guid.NewGuid().ToString("N");

        [Required]
        public DateTime CreatedDate { get; set; }
        
        [Required]
        public DateTime UpdatedDate { get; set; }
    }
}
