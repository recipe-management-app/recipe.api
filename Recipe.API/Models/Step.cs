﻿using System.ComponentModel.DataAnnotations;

namespace Recipe.API.Models
{
    public class Step : Base
    {
        [Required]
        public string RecipeId { get; set; }

        [Range(1, 20)]
        public int No { get; set; }

        [Required, MinLength(3), MaxLength(100)]
        public string Guide { get; set; }

        public Recipe Recipe { get; set; }
    }
}
