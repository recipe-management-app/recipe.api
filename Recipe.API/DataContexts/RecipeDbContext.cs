﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Recipe.API.Models;

namespace Recipe.API.DataContexts
{
    public class RecipeDbContext : DbContext
    {
        public RecipeDbContext()
        {
        }

        public RecipeDbContext(DbContextOptions<RecipeDbContext> options)
            : base(options) => this.Database.Migrate();

        public DbSet<Models.Recipe> Recipes { get; set; }
        public DbSet<Step> Steps { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Photo> Photos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.Recipe>()
                .HasMany(e => e.Ingredients)
                .WithOne(e => e.Recipe)
                .HasForeignKey(e => e.RecipeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Models.Recipe>()
                .HasMany(e => e.Steps)
                .WithOne(e => e.Recipe)
                .HasForeignKey(e => e.RecipeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Models.Recipe>()
                .HasMany(e => e.Photos)
                .WithOne(e => e.Recipe)
                .HasForeignKey(e => e.RecipeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Ingredient>()
                .HasOne(e => e.Recipe)
                .WithMany(e => e.Ingredients)
                .HasForeignKey(e => e.RecipeId);

            modelBuilder.Entity<Step>()
                .HasOne(e => e.Recipe)
                .WithMany(e => e.Steps)
                .HasForeignKey(e => e.RecipeId);

            modelBuilder.Entity<Photo>()
                .HasOne(e => e.Recipe)
                .WithMany(e => e.Photos)
                .HasForeignKey(e => e.RecipeId);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            SaveHook();
            return await base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            SaveHook();
            return SaveChanges();
        }

        public void SaveHook()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is Base && (e.State == EntityState.Added || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((Base)entityEntry.Entity).UpdatedDate = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((Base)entityEntry.Entity).CreatedDate = DateTime.Now;
                }
            }
        }
    }
}
