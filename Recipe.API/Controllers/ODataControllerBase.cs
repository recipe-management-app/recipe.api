﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Recipe.API.DataContexts;
using Recipe.API.Models;

namespace Recipe.API.Controllers
{
	[Produces("application/json")]
	public class ODataControllerBase<T> : ODataController where T : Base, new()
	{
		private readonly RecipeDbContext _context;

		public ODataControllerBase(RecipeDbContext repository)
		{
			_context = repository;
		}

		[ProducesResponseType(204)]
		[ProducesResponseType(404)]
		public async Task<IActionResult> Delete([FromODataUri] string key)
		{
			var entity = await _context.Set<T>().FindAsync(key);
			if (entity == null)
			{
				return NotFound();
			}
			_context.Set<T>().Remove(entity);
			await _context.SaveChangesAsync();
			return NoContent();
		}

        [EnableQuery(MaxExpansionDepth = 5, PageSize = 10)]
        [ProducesResponseType(404)]
        public IActionResult Get()
        {
            var entity = _context.Set<T>().AsQueryable();
            if (entity == null)
            {
                return NotFound();
            }
            return Ok(entity);
        }

        [HttpGet]
        [EnableQuery(MaxExpansionDepth = 5, PageSize = 10)]
        [ProducesResponseType(404)]
        public IActionResult Get([FromODataUri] string key)
        {
            var entityQueryable = _context.Set<T>().Where(t => t.Id == key).AsQueryable();
            if (!entityQueryable.Any())
            {
                return NotFound();
            }
            return Ok(entityQueryable);
        }

        [AcceptVerbs("PATCH", "MERGE")]
		[EnableQuery(MaxExpansionDepth = 5, PageSize = 10)]
		[ProducesResponseType(400)]
		[ProducesResponseType(404)]
		public async Task<IActionResult> Patch([FromODataUri] string key, [FromBody] JsonPatchDocument<T> patch)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			var entity = await _context.Set<T>().FindAsync(key);
			if (entity == null)
			{
				return NotFound();
			}
			patch.ApplyTo(entity, ModelState);
			if (!ModelState.IsValid)
			{
				return new BadRequestObjectResult(ModelState);
			}
			try
			{
				_context.Set<T>().Update(entity);
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (await _context.Set<T>().FindAsync(key) == null)
				{
					return NotFound();
				}
				throw;
			}
			return Ok(await _context.Set<T>().FindAsync(key));
		}

		[HttpPost]
		[ProducesResponseType(400)]
		public async Task<IActionResult> Post([FromBody] T recipe)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			_context.Add(recipe);
			await _context.SaveChangesAsync();
			return Ok(await _context.Set<T>().FindAsync(recipe.Id));
		}

		[HttpPut]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		public async Task<IActionResult> Put([FromODataUri] string key, [FromBody] T recipe)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			if (!key.Equals(recipe.Id))
			{
				return BadRequest();
			}
			try
			{
				_context.Set<T>().Update(recipe);
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (await _context.Set<T>().FindAsync(key) == null)
				{
					return NotFound();
				}
				throw;
			}
			return Ok(await _context.Set<T>().FindAsync(key));
		}
	}
}
