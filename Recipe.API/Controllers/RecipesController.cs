﻿using Recipe.API.DataContexts;
using Recipe.API.Models;

namespace Recipe.API.Controllers
{
	public class RecipesController : ODataControllerBase<Models.Recipe>
	{
		public RecipesController(RecipeDbContext repository) : base(repository)
		{
		}
	}
}
