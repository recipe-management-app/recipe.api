﻿using Recipe.API.DataContexts;
using Recipe.API.Models;

namespace Recipe.API.Controllers
{
    public class StepsController : ODataControllerBase<Step>
	{
		public StepsController(RecipeDbContext repository) : base(repository)
		{
		}
	}
}
