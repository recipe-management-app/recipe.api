﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recipe.API.Controllers
{
    [Route("static/[controller]")]
    [ApiController]
    public class TypesController : ControllerBase
    {
        // GET: api/<TypesController1>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Enum.GetNames(typeof(API.Models.Type));
        }
    }

    [Route("static/[controller]")]
    [ApiController]
    public class UnitsController : ControllerBase
    {
        // GET: api/<TypesController1>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Enum.GetNames(typeof(API.Models.Unit));
        }
    }
}
