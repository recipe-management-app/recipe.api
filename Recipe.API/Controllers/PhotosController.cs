﻿using Recipe.API.DataContexts;
using Recipe.API.Models;

namespace Recipe.API.Controllers
{
    public class PhotosController : ODataControllerBase<Photo>
	{
		public PhotosController(RecipeDbContext repository) : base(repository)
		{
		}
	}
}
