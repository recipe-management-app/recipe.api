﻿using Recipe.API.DataContexts;
using Recipe.API.Models;

namespace Recipe.API.Controllers
{
    public class IngredientsController : ODataControllerBase<Ingredient>
	{
		public IngredientsController(RecipeDbContext repository) : base(repository)
		{
		}
	}
}
