﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OData.Edm;
using Recipe.API.DataContexts;
using Recipe.API.Models;

namespace Recipe.API
{
    public class Startup
    {
        public readonly string[] _clientOrigins;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var section = Configuration.GetSection("ClientOrigins");
            _clientOrigins = section.Get<string[]>();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<RecipeDbContext>(context => context.UseMySql(connectionString));

            services.AddOData();
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            });
            // Make sure call this previous to AddMvc
            services.AddCors(options => {
                options.AddPolicy("AllowSpecificOrigin", builder => 
                    builder.WithOrigins(_clientOrigins)
                        .SetIsOriginAllowed(o => true)
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });
            services.AddMvc(options =>
            {
                // https://github.com/Microsoft/aspnet-api-versioning/issues/361
                // Because conflicts with ODataRouting as of this version could improve performance though
                options.EnableEndpointRouting = false;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Make sure you call this before calling app.UseMvc()
            app.UseCors("AllowSpecificOrigin");
            app.UseMvc(b =>
            {
                b.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
                b.MapODataServiceRoute("Recipes", "odata", GetEdmModel());
            });
        }

        private static IEdmModel GetEdmModel()
        {
            var edmModelBuilder = new ODataConventionModelBuilder();
            edmModelBuilder.EntitySet<Models.Recipe>("Recipes");
            edmModelBuilder.EntitySet<Ingredient>("Ingredients");
            edmModelBuilder.EntitySet<Step>("Steps");
            edmModelBuilder.EntitySet<Photo>("Photos");

            return edmModelBuilder.GetEdmModel();
        }
    }
}
